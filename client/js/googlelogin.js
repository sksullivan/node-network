$(document).ready(function () {
	$.ajaxSetup({ cache: true });
	googleSetup();
});

function googleSetup () {
	setLoginButton = function () {
		gapi.signin.render("subsearch", { // This makes the DOM element with id = "subsearch" become the login button for google
			'callback': googleCallback,
			'clientid': '596296348321-enk40knkm6nfj79k3pjgk9shee3uqsop.apps.googleusercontent.com',
			'cookiepolicy': 'single_host_origin',
			'scope': 'https://www.googleapis.com/auth/drive'
		});
	}

	var po = document.createElement('script');
	po.type = 'text/javascript';
	po.async = false;
	po.src = 'https://plus.google.com/js/client:plusone.js?onload=setLoginButton';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(po, s);
}

function googleCallback (googleAuthResult) {

	if (googleAuthResult['code']) {// If we got a good response back
		$.get('/googleauth', { code: googleAuthResult['code'] }, function (data) { // Send that code to the server so the server can exchange it for an access token
			if (data != "error") {
				alert("Logged into Google!");
			} else {
				alert("Server error logging into Google.");
			}
		});
	} else {
		console.log("GOOGLE LOGIN ERROR: "+googleAuthResult['error']);
	}
}