var socket = io.connect(window.location.origin);

recipient = null;
online = null;
me = {
	username: null,
	ip: null,
	socketIndex: null
};

socket.on('reloadUsers', function (data) {
	online = data.onlineUsers;
	string = "";
	$('#users').empty();
	for (i=0;i<data.onlineUsers.length;i++) {
		user = data.onlineUsers[i];
		$('#users').append($("<li>", {
			id: "recipient"+i,
			text: user.username+" "+user.ip,
			onclick: "setRecipient('recipient"+i+"','"+user.username+"','"+user.ip+"')"
		}));
	}
});

socket.on('pickName', function (data) {
	myUsername = prompt("Pick a username for Bubbl chat.");
	socket.emit('usernameResponse', { username: myUsername });
});

socket.on('youAre', function (data) {
	me = data.user;
});

socket.on('welcomeBack', function (data) {
	alert("Welcome back, "+data.username);
});

socket.on('chat', function (data) {
	$('#content').append('<br>'+data.from.username+" ["+data.from.ip+"]: "+data.message);
});

function send () {
	if (recipient != null) {
		socket.emit('chat', {
			from: me,
			message: $('#message').val(),
			to: recipient
		});
		$('#content').append('<br>'+me.username+" ["+me.ip+"]: "+$('#message').val());
	} else {
		alert("Please select a recipient!");
	}
}

function setRecipient (id,name,addr) {
	for (i=0;i<online.length;i++) {
		$('#recipient'+i).css({
			color: "#000000"
		});
	}
	$('#'+id).css({
		color: "#FF0000"
	});
	recipient = {
		username: name,
		ip: addr
	};
}