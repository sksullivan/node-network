function register() {
	firstName = $('#firstName').val();
	lastName = $('#lastName').val();
	username = $('#username').val();
	profileUrl = $('#profileUrl').val();
	password = $('#password').val();
	password2 = $('#password2').val();
	if (password != password2) {
		alert("Please double check that you typed your password correctly.");
		return;
	}
	$.post('//west-egg-candy.herokuapp.com/register', {
		firstName: firstName,
		lastName: lastName,
		profileUrl: profileUrl,
		username: username,
		password: password,
	}, function (data) {
		alert(data);
	});
}

function login() {
	username = $('#usernameLogin').val();
	password = $('#passwordLogin').val();
	$.post('//west-egg-candy.herokuapp.com/login', {
		username: username,
		password: password
	}, function (data) {
		if (data != "good") {
			alert("bad uname/pass");
		} else {
			alert("logged in");
		}
	});
}