// MODULES ------------------------------------

express = require('express');
colors = require('colors');
pg = require('pg');
crypto = require('crypto');
request = require('request');

// SETUP ------------------------------------

app = express();

server = require('http').createServer(app);
io = require('socket.io').listen(server);

logger = function(req, res, next) {
    console.log("   info (backend) - ".cyan+req.method+" "+req.url);
    next();
}

app.configure(function () {
	app.set('views', __dirname + '/client/views');
	app.set('view engine', 'jade');
	app.use(express.cookieParser());
	app.use(express.json());
	app.use(express.urlencoded());
	app.use(express.methodOverride());
	app.use(logger);
	app.use(app.router);
	app.use(express.static(__dirname + '/client'));
	app.use(function (req,res) {
		res.send('404 - Not found');
		console.log('   error (backend) - '.red+'client requested an undefined route :(');
	});
});

app.configure('development', function () {
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});


// PAGE ROUTES ------------------------------------

app.get('/', function (req,res) {
	res.render("index", {
		title: 'index'
	});
});

app.get('/redirect', function (req,res) {
	res.render("redirectDelay", {
		title: req.query.title,
		path: req.query.path,
		message: req.query.message
	});
});

app.get('/users/browse', function (req,res) {
	pg.connect(process.env.DATABASE_URL, function (err, client, done) {
		client.query("SELECT * FROM Users;", function (err, result) {
			users = [];
			if (err) {
				return console.error(err);
			}
			if (result) {
				for (i=0;i<result.rowCount;i++) {
					users.push({
						id: result.rows[i].id,
						profileUrl: result.rows[i].data.profileUrl,
						name: result.rows[i].data.firstName+" "+result.rows[i].data.lastName // Databse.get name for req.params.userID
					});
				}
				done();
				console.log(users);
				res.render("browseUsers", {
					title: 'Browse Users',
					data: users
				});
			} else {
				res.redirect("/redirect?title=No Users Found&path=/&message=No users found, sorry");
			}
		});
	});
});

app.get('/users/:userId', function (req,res) {
	if (req.params.userId == "me") {
		for (i=0;i<onlineUsers.length;i++) {
			console.log(onlineUsers[i].passwordHash);
			console.log(req.cookies.loginToken);
			if (onlineUsers[i].passwordHash = req.cookies.loginToken) {
				console.log(onlineUsers[i].data);
				res.render("user", {
					title: "User: "+req.params.userId,
					profileUrl: onlineUsers[i].data.profileUrl,
					name: onlineUsers[i].data.firstName+" "+onlineUsers[i].data.lastName // Databse.get name for req.params.userID
				});
				return;
			}
		}
		res.redirect("/redirect?title=Please Login&path=/&message=To view your own profile, you need to be logged in!");
	}
	pg.connect(process.env.DATABASE_URL, function (err, client, done) {
		client.query("SELECT * FROM Users WHERE id="+req.params.userId+";", function (err, result) {
			if (result) {
				res.render("user", {
					title: "User: "+result.rows[0].username,
					profileUrl: result.rows[0].data.profileUrl,
					name: result.rows[0].data.firstName+" "+result.rows[0].data.lastName
				});
			} else {
				res.render("user", {
					title: "User Not Found",
					profileUrl: "http://media.lightingnewyork.com/grfx/micro/sad_face.png",
					name: "Sorry, that user ID doesn't correspond to anyone using DormBizz"
				});
			}
			done();
			if (err) {
				return console.error(err);
			}
		});
	});
});

app.get('/projects/browse', function (req,res) {
	res.render("browseProjects", {
		title: 'Browse Projects'
	});
});

app.get('/projects/:projectId', function (req,res) {
	res.render("project", {
		title: "Project: "+req.params.userId
	});
});

app.get('/login', function (req,res) {
	res.render("login", {
		title: "login"
	});
});

app.get('/logout', function (req,res) {
	for (i=0;i<onlineUsers.length;i++) {
		if (onlineUsers[i].passwordHash == req.cookies.loginToken) {
			onlineUsers.pop(onlineUsers[i]);
		}
	}
	console.log(onlineUsers);
	res.clearCookie('loginToken');
	res.redirect("/redirect?title=Logged Out&path=/&message=Logged out!");
});

app.post('/login', function (req,res) {
	console.log(req.body);
	pg.connect(process.env.DATABASE_URL, function (err, client, done) {
		client.query("SELECT * FROM Users WHERE Username='"+req.body.username+"' AND Password='"+req.body.password+"';", function (err, result) {
			if (result.rowCount > 0) {
				console.log(result);
				shasum = crypto.createHash('sha1');
				shasum.update(req.body.password);
				hash = shasum.digest('hex');
				res.cookie('loginToken', hash, { maxAge: 900000, httpOnly: true});
				onlineUsers.push({
					username: req.body.username,
					passwordHash: hash,
					data: result.rows[0].data,
					id: result.rows[0].id
				});
				res.send("good");
			} else {
				res.send("bad uname/pass");
			}
			done();
			if (err) {
				return console.error(err);
			}
		});
	});
});

app.post('/register', function (req,res) {
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query("SELECT * FROM Users WHERE Username='"+req.body.username+"';", function (err, result) {
			if (result.rowCount > 0) {
				res.send("User already exists.");
			} else {
				client.query("SELECT COUNT(*) FROM Users;", function (err, result) {
					console.log(result);
					if (err) {
						console.error("from get count");
						return console.error(err);
					}
					console.log(parseInt(result.rows[0].count)+1);
					data = '{"firstName": "'+req.body.firstName+'","lastName": "'+req.body.lastName+'","profileUrl": "'+req.body.profileUrl+'"}'
					client.query("INSERT INTO Users (id, Data, Username, Password) VALUES ('"+(parseInt(result.rows[0].count)+1)+"', '"+data+"','"+req.body.username+"','"+req.body.password+"');", function (err, result) {
						if (err) {
							console.error("from insert");
							return console.error(err);
						}
						res.send("Registered!");
					});
				});
			}
			done();
			if (err) {
				console.error("from check");
				return console.error(err);
			}
		});
	});
});

//ROUTE FOR LOGIN

app.get('/favicon.ico', function (req,res) {
	res.sendfile('/assets/favicon.ico')
});

// START SERVER ------------------------------------

server.listen(process.env.PORT || 5000);
//app.listen(process.env.PORT || 5000);
